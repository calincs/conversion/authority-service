/*
This file contains a cache of all the contemporary CIDOC-CRM classes. 

A table displaying the 81 Classes and the 160 Properties declared in CIDOC-CRM version 7.1.2 
can be found at https://www.cidoc-crm.org/html/cidoc_crm_v7.1.2.html
*/

export const crm_classes = {
    "E1_CRM_Entity": {
        "id": "http://www.cidoc-crm.org/cidoc-crm/E1_CRM_Entity",
        "name": "CRM Entity",
        "description": "This class comprises all things in the universe of discourse of the CIDOC Conceptual Reference Model."
    },
    "E2_Temporal_Entity": {
        "id": "http://www.cidoc-crm.org/cidoc-crm/E2_Temporal_Entity",
        "name": "Temporal Entity",
        "description": "This class comprises all phenomena, such as the instances of E4 Periods and E5 Events, which happen over a limited extent in time."
    },
    "E3_Condition_State": {
        "id": "http://www.cidoc-crm.org/cidoc-crm/E3_Condition_State",
        "name": "Condition State",
        "description": "This class comprises the states of objects characterised by a certain condition over a time-span."
    },
    "E4_Period": {
        "id": "http://www.cidoc-crm.org/cidoc-crm/E4_Period",
        "name": "Period",
        "description": "This class comprises sets of coherent phenomena or cultural manifestations occurring in time and space."
    },
    "E5_Event": {
        "id": "http://www.cidoc-crm.org/cidoc-crm/E5_Event",
        "name": "Event",
        "description": "This class comprises distinct, delimited and coherent processes and interactions of a material nature, in cultural, social or physical systems, involving and affecting instances of E77 Persistent Item in a way characteristic of the kind of process."
    },
    "E6_Destruction": {
        "id": "http://www.cidoc-crm.org/cidoc-crm/E6_Destruction",
        "name": "Destruction",
        "description": "This class comprises events that destroy one or more instances of E18 Physical Thing such that they lose their identity as the subjects of documentation."
    },
    "E7_Activity": {
        "id": "http://www.cidoc-crm.org/cidoc-crm/E7_Activity",
        "name": "Activity",
        "description": "This class comprises actions intentionally carried out by instances of E39 Actor that result in changes of state in the cultural, social, or physical systems documented."
    },
    "E8_Acquisition": {
        "id": "http://www.cidoc-crm.org/cidoc-crm/E8_Acquisition",
        "name": "Acquisition",
        "description": "This class comprises transfers of legal ownership from one or more instances of E39 Actor to one or more other instances of E39 Actor."
    },
    "E9_Move": {
        "id": "http://www.cidoc-crm.org/cidoc-crm/E9_Move",
        "name": "Move",
        "description": "This class comprises changes of the physical location of the instances of E19 Physical Object."
    },
    "E10_Transfer_of_Custody": {
        "id": "http://www.cidoc-crm.org/cidoc-crm/E10_Transfer_of_Custody",
        "name": "Transfer of Custody",
        "description": "This class comprises transfers of the physical custody or the legal responsibility for the physical custody of objects. The recording of the donor or recipient is optional."
    },
    "E11_Modification": {
        "id": "http://www.cidoc-crm.org/cidoc-crm/E11_Modification",
        "name": "Modification",
        "description": "This class comprises instances of E7 Activity that are undertaken to create, alter or change instances of E24 Physical Human-Made Thing."
    },
    "E12_Production": {
        "id": "http://www.cidoc-crm.org/cidoc-crm/E12_Production",
        "name": "Production",
        "description": "This class comprises activities that are designed to, and succeed in, creating one or more new items."
    },
    "E13_Attribute_Assignment": {
        "id": "http://www.cidoc-crm.org/cidoc-crm/E13_Attribute_Assignment",
        "name": "Attribute Assignment",
        "description": "This class comprises the actions of making assertions about one property of an object or any single relation between two items or concepts. The type of the property asserted to hold between two items or concepts can be described by the property P177 assigned property of type (is type of property assigned): E55 Type."
    },
    "E14_Condition_Assessment": {
        "id": "http://www.cidoc-crm.org/cidoc-crm/E14_Condition_Assessment",
        "name": "Condition Assessment",
        "description": "This class describes the act of assessing the state of preservation of an object during a particular period."
    },
    "E15_Identifier_Assignment": {
        "id": "http://www.cidoc-crm.org/cidoc-crm/E15_Identifier_Assignment",
        "name": "Identifier Assignment",
        "description": "This class comprises activities that result in the allocation of an identifier to an instance of E1 CRM Entity. Instances of E15 Identifier Assignment may include the creation of the identifier from multiple constituents, which themselves may be instances of E41 Appellation."
    },
    "E16_Measurement": {
        "id": "http://www.cidoc-crm.org/cidoc-crm/E16_Measurement",
        "name": "Measurement",
        "description": "This class comprises actions measuring quantitative physical properties and other values that can be determined by a systematic, objective procedure of direct observation of particular states of physical reality."
    },
    "E17_Type_Assignment": {
        "id": "http://www.cidoc-crm.org/cidoc-crm/E17_Type_Assignment",
        "name": "Type Assignment",
        "description": "This class comprises the actions of classifying items of whatever kind. Such items include objects, specimens, people, actions and concepts."
    },
    "E18_Physical_Thing": {
        "id": "http://www.cidoc-crm.org/cidoc-crm/E18_Physical_Thing",
        "name": "Physical Thing",
        "description": "This class comprises all persistent physical items with a relatively stable form, human-made or natural."
    },
    "E19_Physical_Object": {
        "id": "http://www.cidoc-crm.org/cidoc-crm/E19_Physical_Object",
        "name": "Physical Object",
        "description": "This class comprises items of a material nature that are units for documentation and have physical boundaries that separate them completely in an objective way from other objects."
    },
    "E20_Biological_Object": {
        "id": "http://www.cidoc-crm.org/cidoc-crm/E20_Biological_Object",
        "name": "Biological Object",
        "description": "This class comprises individual items of a material nature, which live, have lived or are natural products of or from living organisms."
    },
    "E21_Person": {
        "id": "http://www.cidoc-crm.org/cidoc-crm/E21_Person",
        "name": "Person",
        "description": "This class comprises real persons who live or are assumed to have lived."
    },
    "E22_Human-Made_Object": {
        "id": "http://www.cidoc-crm.org/cidoc-crm/E22_Human-Made_Object",
        "name": "Human-Made Object",
        "description": "This class comprises all persistent physical objects of any size that are purposely created by human activity and have physical boundaries that separate them completely in an objective way from other objects."
    },
    "E24_Physical_Human-Made_Thing": {
        "id": "http://www.cidoc-crm.org/cidoc-crm/E24_Physical_Human-Made_Thing",
        "name": "Physical Human-Made Thing",
        "description": "This class comprises all persistent physical items of any size that are purposely created by human activity."
    },
    "E25_Human-Made_Feature": {
        "id": "http://www.cidoc-crm.org/cidoc-crm/E25_Human-Made_Feature",
        "name": "Human-Made Feature",
        "description": "This class comprises physical features that are purposely created by human activity, such as scratches, artificial caves, artificial water channels, etc. In particular, it includes the information encoding features on mechanical or digital carriers."
    },
    "E26_Physical_Feature": {
        "id": "http://www.cidoc-crm.org/cidoc-crm/E26_Physical_Feature",
        "name": "Physical Feature",
        "description": "This class comprises identifiable features that are physically attached in an integral way to particular physical objects."
    },
    "E27_Site": {
        "id": "http://www.cidoc-crm.org/cidoc-crm/E27_Site",
        "name": "Site",
        "description": "This class comprises pieces of land or sea floor."
    },
    "E28_Conceptual_Object": {
        "id": "http://www.cidoc-crm.org/cidoc-crm/E28_Conceptual_Object",
        "name": "Conceptual Object",
        "description": "This class comprises non-material products of our minds and other human produced data that have become objects of a discourse about their identity, circumstances of creation or historical implication."
    },
    "E29_Design_or_Procedure": {
        "id": "http://www.cidoc-crm.org/cidoc-crm/E29_Design_or_Procedure",
        "name": "Design or Procedure",
        "description": "This class comprises documented plans for the execution of actions in order to achieve a result of a specific quality, form or contents."
    },
    "E30_Right": {
        "id": "http://www.cidoc-crm.org/cidoc-crm/E30_Right",
        "name": "Right",
        "description": "This class comprises legal privileges concerning material and immaterial things or their derivatives."
    },
    "E31_Document": {
        "id": "http://www.cidoc-crm.org/cidoc-crm/E31_Document",
        "name": "Document",
        "description": "This class comprises identifiable immaterial items that make propositions about reality."
    },
    "E32_Authority_Document": {
        "id": "http://www.cidoc-crm.org/cidoc-crm/E32_Authority_Document",
        "name": "Authority Document",
        "description": "This class comprises encyclopaedia, thesauri, authority lists and other documents that define terminology or conceptual systems for consistent use."
    },
    "E33_Linguistic_Object": {
        "id": "http://www.cidoc-crm.org/cidoc-crm/E33_Linguistic_Object",
        "name": "Linguistic Object",
        "description": "This class comprises identifiable expressions in natural language or languages."
    },
    "E34_Inscription": {
        "id": "http://www.cidoc-crm.org/cidoc-crm/E34_Inscription",
        "name": "Inscription",
        "description": "This class comprises recognisable, texts attached to instances of E24 Physical Human-Made Thing."
    },
    "E35_Title": {
        "id": "http://www.cidoc-crm.org/cidoc-crm/E35_Title",
        "name": "Title",
        "description": "This class comprises textual strings that within a cultural context can be clearly identified as titles due to their form."
    },
    "E36_Visual_Item": {
        "id": "http://www.cidoc-crm.org/cidoc-crm/E36_Visual_Item",
        "name": "Visual Item",
        "description": "This class comprises the intellectual or conceptual aspects of recognisable marks, images and other visual works."
    },
    "E37_Mark": {
        "id": "http://www.cidoc-crm.org/cidoc-crm/E37_Mark",
        "name": "Mark",
        "description": "This class comprises symbols, signs, signatures or short texts applied to instances of E24 Physical Human-Made Thing by arbitrary techniques, often in order to indicate such things as creator, owner, dedications, purpose or to communicate information generally."
    },
    "E39_Actor": {
        "id": "http://www.cidoc-crm.org/cidoc-crm/E39_Actor",
        "name": "Actor",
        "description": "This class comprises people, either individually or in groups, who have the potential to perform intentional actions of kinds for which someone may be held responsible."
    },
    "E41_Appellation": {
        "id": "http://www.cidoc-crm.org/cidoc-crm/E41_Appellation",
        "name": "Appellation",
        "description": "This class comprises signs, either meaningful or not, or arrangements of signs following a specific syntax, that are used or can be used to refer to and identify a specific instance of some class or category within a certain context."
    },
    "E42_Identifier": {
        "id": "http://www.cidoc-crm.org/cidoc-crm/E42_Identifier",
        "name": "Identifier",
        "description": "This class comprises strings or codes assigned to instances of E1 CRM Entity in order to identify them uniquely and permanently within the context of one or more organisations."
    },
    "E52_Time-Span": {
        "id": "http://www.cidoc-crm.org/cidoc-crm/E52_Time-Span",
        "name": "Time-Span",
        "description": "This class comprises abstract temporal extents, in the sense of Galilean physics, having a beginning, an end and a duration."
    },
    "E53_Place": {
        "id": "http://www.cidoc-crm.org/cidoc-crm/E53_Place",
        "name": "Place",
        "description": "This class comprises extents in the natural space we live in, in particular on the surface of the Earth, in the pure sense of physics: independent from temporal phenomena and matter."
    },
    "E54_Dimension": {
        "id": "http://www.cidoc-crm.org/cidoc-crm/E54_Dimension",
        "name": "Dimension",
        "description": "This class comprises quantifiable properties that can be measured by some calibrated means and can be approximated by values, i.e., by points or regions in a mathematical or conceptual space, such as natural or real numbers, RGB values etc."
    },
    "E55_Type": {
        "id": "http://www.cidoc-crm.org/cidoc-crm/E55_Type",
        "name": "Type",
        "description": "This class comprises concepts denoted by terms from thesauri and controlled vocabularies used to characterize and classify instances of CIDOC CRM classes."
    },
    "E56_Language": {
        "id": "http://www.cidoc-crm.org/cidoc-crm/E56_Language",
        "name": "Language",
        "description": "This class is a specialization of E55 Type and comprises the natural languages in the sense of concepts."
    },
    "E57_Material": {
        "id": "http://www.cidoc-crm.org/cidoc-crm/E57_Material",
        "name": "Material",
        "description": "This class is a specialization of E55 Type and comprises the concepts of materials."
    },
    "E58_Measurement_Unit": {
        "id": "http://www.cidoc-crm.org/cidoc-crm/E58_Measurement_Unit",
        "name": "Measurement Unit",
        "description": "This class is a specialization of E55 Type and comprises the types of measurement units: feet, inches, centimetres, litres, lumens, etc."
    },
    "E59_Primitive_Value": {
        "id": "http://www.cidoc-crm.org/cidoc-crm/E59_Primitive_Value",
        "name": "Primitive Value",
        "description": "This class comprises values of primitive data types of programming languages or database management systems and data types composed of such values used as documentation elements, as well as their mathematical abstractions."
    },
    "E60_Number": {
        "id": "http://www.cidoc-crm.org/cidoc-crm/E60_Number",
        "name": "Number",
        "description": "This class comprises any encoding of computable (algebraic) values such as integers, real numbers, complex numbers, vectors, tensors etc., including intervals of these values to express limited precision."
    },
    "E61_Time_Primitive": {
        "id": "http://www.cidoc-crm.org/cidoc-crm/E61_Time_Primitive",
        "name": "Time Primitive",
        "description": "This class comprises instances of E59 Primitive Value for time that should be implemented with appropriate validation, precision and references to temporal coordinate systems to express time in some context relevant to cultural and scientific documentation."
    },
    "E62_String": {
        "id": "http://www.cidoc-crm.org/cidoc-crm/E62_String",
        "name": "String",
        "description": "This class comprises coherent sequences of binary-encoded symbols. They correspond to the content of an instance of E90 Symbolic object."
    },
    "E63_Beginning_of_Existence": {
        "id": "http://www.cidoc-crm.org/cidoc-crm/E63_Beginning_of_Existence",
        "name": "Beginning of Existence",
        "description": "This class comprises events that bring into existence any instance of E77 Persistent Item."
    },
    "E64_End_of_Existence": {
        "id": "http://www.cidoc-crm.org/cidoc-crm/E64_End_of_Existence",
        "name": "End of Existence",
        "description": "This class comprises events that end the existence of any instance of E77 Persistent Item."
    },
    "E65_Creation": {
        "id": "http://www.cidoc-crm.org/cidoc-crm/E65_Creation",
        "name": "Creation",
        "description": "This class comprises events that result in the creation of conceptual items or immaterial products, such as legends, poems, texts, music, images, movies, laws, types etc."
    },
    "E66_Formation": {
        "id": "http://www.cidoc-crm.org/cidoc-crm/E66_Formation",
        "name": "Formation",
        "description": "This class comprises events that result in the formation of a formal or informal E74 Group of people, such as a club, society, association, corporation or nation."
    },
    "E67_Birth": {
        "id": "http://www.cidoc-crm.org/cidoc-crm/E67_Birth",
        "name": "Birth",
        "description": "This class comprises the births of human beings. E67 Birth is a biological event focussing on the context of people coming into life."
    },
    "E68_Dissolution": {
        "id": "http://www.cidoc-crm.org/cidoc-crm/E68_Dissolution",
        "name": "Dissolution",
        "description": "This class comprises the events that result in the formal or informal termination of an instance of E74 Group."
    },
    "E69_Death": {
        "id": "http://www.cidoc-crm.org/cidoc-crm/E69_Death",
        "name": "Death",
        "description": "This class comprises the deaths of human beings."
    },
    "E70_Thing": {
        "id": "http://www.cidoc-crm.org/cidoc-crm/E70_Thing",
        "name": "Thing",
        "description": "This general class comprises discrete, identifiable, instances of E77 Persistent Item that are documented as single units, that either consist of matter or depend on being carried by matter and are characterized by relative stability."
    },
    "E71_Human-Made_Thing": {
        "id": "http://www.cidoc-crm.org/cidoc-crm/E71_Human-Made_Thing",
        "name": "Human-Made Thing",
        "description": "This class comprises discrete, identifiable human-made items that are documented as single units."
    },
    "E72_Legal_Object": {
        "id": "http://www.cidoc-crm.org/cidoc-crm/E72_Legal_Object",
        "name": "Legal Object",
        "description": "This class comprises those material or immaterial items to which instances of E30 Right, such as the right of ownership or use, can be applied."
    },
    "E73_Information_Object": {
        "id": "http://www.cidoc-crm.org/cidoc-crm/E73_Information_Object",
        "name": "Information Object",
        "description": "This class comprises identifiable immaterial items, such as poems, jokes, data sets, images, texts, multimedia objects, procedural prescriptions, computer program code, algorithm or mathematical formulae, that have an objectively recognizable structure and are documented as single units."
    },
    "E74_Group": {
        "id": "http://www.cidoc-crm.org/cidoc-crm/E74_Group",
        "name": "Group",
        "description": "This class comprises any gatherings or organizations of human individuals or groups that act collectively or in a similar way due to any form of unifying relationship."
    },
    "E77_Persistent_Item": {
        "id": "http://www.cidoc-crm.org/cidoc-crm/E77_Persistent_Item",
        "name": "Persistent Item",
        "description": "This class comprises items that have persistent characteristics of structural nature substantially related to their identity and their integrity, sometimes known as “endurants” in philosophy."
    },
    "E78_Curated_Holding": {
        "id": "http://www.cidoc-crm.org/cidoc-crm/E78_Curated_Holding",
        "name": "Curated Holding",
        "description": "This class comprises aggregations of instances of E18 Physical Thing that are assembled and maintained (“curated” and “preserved,” in museological terminology) by one or more instances of E39 Actor over time for a specific purpose and audience, and according to a particular collection development plan."
    },
    "E79_Part_Addition": {
        "id": "http://www.cidoc-crm.org/cidoc-crm/E79_Part_Addition",
        "name": "Part Addition",
        "description": "This class comprises activities that result in an instance of E18 Physical Thing being increased, enlarged or augmented by the addition of a part."
    },
    "E80_Part_Removal": {
        "id": "http://www.cidoc-crm.org/cidoc-crm/E80_Part_Removal",
        "name": "Part Removal",
        "description": "This class comprises the activities that result in an instance of E18 Physical Thing being decreased by the removal of a part."
    },
    "E81_Transformation": {
        "id": "http://www.cidoc-crm.org/cidoc-crm/E81_Transformation",
        "name": "Transformation",
        "description": "This class comprises the events that result in the simultaneous destruction of one or more than one E18 Physical Thing and the creation of one or more than one E18 Physical Thing that preserves recognizable substance and structure from the first one(s) but has fundamentally different nature or identity."
    },
    "E83_Type_Creation": {
        "id": "http://www.cidoc-crm.org/cidoc-crm/E83_Type_Creation",
        "name": "Type Creation",
        "description": "This class comprises activities formally defining new types of items."
    },
    "E85_Joining": {
        "id": "http://www.cidoc-crm.org/cidoc-crm/E85_Joining",
        "name": "Joining",
        "description": "This class comprises the activities that result in an instance of E39 Actor becoming a member of an instance of E74 Group."
    },
    "E86_Leaving": {
        "id": "http://www.cidoc-crm.org/cidoc-crm/E86_Leaving",
        "name": "Leaving",
        "description": "This class comprises the activities that result in an instance of E39 Actor to be disassociated from an instance of E74 Group."
    },
    "E87_Curation_Activity": {
        "id": "http://www.cidoc-crm.org/cidoc-crm/E87_Curation_Activity",
        "name": "Curation Activity",
        "description": "This class comprises the activities that result in the continuity of management and the preservation and evolution of instances of E78 Curated Holding, following an implicit or explicit curation plan."
    },
    "E89_Propositional_Object": {
        "id": "http://www.cidoc-crm.org/cidoc-crm/E89_Propositional_Object",
        "name": "Propositional Object",
        "description": "This class comprises immaterial items, including but not limited to stories, plots, procedural prescriptions, algorithms, laws of physics or images that are, or represent in some sense, sets of propositions about real or imaginary things and that are documented as single units or serve as topic of discourse."
    },
    "E90_Symbolic_Object": {
        "id": "http://www.cidoc-crm.org/cidoc-crm/E90_Symbolic_Object",
        "name": "Symbolic Object",
        "description": "This class comprises identifiable symbols and any aggregation of symbols, such as characters, identifiers, traffic signs, emblems, texts, data sets, images, musical scores, multimedia objects, computer program code or mathematical formulae that have an objectively recognizable structure and that are documented as single units."
    },
    "E92_Spacetime_Volume": {
        "id": "http://www.cidoc-crm.org/cidoc-crm/E92_Spacetime_Volume",
        "name": "Spacetime Volume",
        "description": "This class comprises 4-dimensional point sets (volumes) in physical spacetime (in contrast to mathematical models of it) regardless their true geometric forms."
    },
    "E93_Presence": {
        "id": "http://www.cidoc-crm.org/cidoc-crm/E93_Presence",
        "name": "Presence",
        "description": "This class comprises instances of E92 Spacetime Volume, whose temporal extent has been chosen in order to determine the spatial extent of a phenomenon over the chosen time-span."
    },
    "E94_Space_Primitive": {
        "id": "http://www.cidoc-crm.org/cidoc-crm/E94_Space_Primitive",
        "name": "Space Primitive",
        "description": "This class comprises instances of E59 Primitive Value for space that should be implemented with appropriate validation, precision and references to spatial coordinate systems to express geometries on or relative to Earth, or on any other stable constellations of matter, relevant to cultural and scientific documentation."
    },
    "E95_Spacetime_Primitive": {
        "id": "http://www.cidoc-crm.org/cidoc-crm/E95_Spacetime_Primitive",
        "name": "Spacetime Primitive",
        "description": "This class comprises instances of E59 Primitive Value for spacetime volumes that should be implemented with appropriate validation, precision and reference systems to express geometries being limited and varying over time on or relative to Earth, or any other stable constellations of matter, relevant to cultural and scientific documentation."
    },
    "E96_Purchase": {
        "id": "http://www.cidoc-crm.org/cidoc-crm/E96_Purchase",
        "name": "Purchase",
        "description": "This class comprises transfers of legal ownership from one or more instances of E39 Actor to one or more different instances of E39 Actor, where the transferring party is completely compensated by the payment of a monetary amount."
    },
    "E97_Monetary_Amount": {
        "id": "http://www.cidoc-crm.org/cidoc-crm/E97_Monetary_Amount",
        "name": "Monetary Amount",
        "description": "This class comprises quantities of monetary possessions or obligations in terms of their nominal value with respect to a particular currency."
    },
    "E98_Currency": {
        "id": "http://www.cidoc-crm.org/cidoc-crm/E98_Currency",
        "name": "Currency",
        "description": "This class comprises the units in which a monetary system, supported by an administrative authority or other community, quantifies and arithmetically compares all monetary amounts declared in the unit."
    },
    "E99_Product_Type": {
        "id": "http://www.cidoc-crm.org/cidoc-crm/E99_Product_Type",
        "name": "Product Type",
        "description": "This class comprises the units in which a monetary system, supported by an administrative authority or other community, quantifies and arithmetically compares all monetary amounts declared in the unit."
    },
    "F1_Work": {
        "id": "http://iflastandards.info/ns/fr/frbr/frbroo/F1_Work",
        "name": "Work",
        "description": "This class comprises the sums of concepts which appear in the course of the coherent evolution of an original idea into one or more expressions that are dominated by the original idea."
    },
    "F2_Expression": {
        "id": "http://iflastandards.info/ns/fr/frbr/frbroo/F2_Expression",
        "name": "Expression",
        "description": "This class comprises the intellectual or artistic realisations of works in the form of identifiable immaterial objects, such as texts, poems, jokes, musical or choreographic notations, movement pattern, sound pattern, images, multimedia objects, or any combination of such forms that have objectively recognisable structures."
    },
    "F4_Manifest": {
        "id":   "http://iflastandards.info/ns/fr/frbr/frbroo/F4_Manifestation_Singleton",
        "name": "Manifestation Singleton",
        "description": "This class comprises physical objects that each carry an instance of F2 Expression, and that were produced as unique objects, with no siblings intended in the course of their production."
    },
    "F28_Expression_Creation": {
        "id":   "http://iflastandards.info/ns/fr/frbr/frbroo/F28_Expression_Creation",
        "name": "Expression Creation",
        "description": "This class comprises activities that result in instances of F2 Expression coming into existence."
    },
    "F29_Recording_Event": {
        "id":   "http://iflastandards.info/ns/fr/frbr/frbroo/F29_Recording_Event",
        "name": "Recording Event",
        "description": "This class comprises activities that intend to convey (and preserve) the content of events in a recording, such as a live recording of a performance, a documentary, or other capture of a perdurant."
    },
    "F31_Performance": {
        "id":  "http://iflastandards.info/ns/fr/frbr/frbroo/F31_Performance",
        "name": "Performance",
        "description": "This class comprises activities that follow the directions of a performance plan, such as a theatrical play, an expression of a choreographic work or a musical work; i.e., they are intended to communicate directly or indirectly to an audience."
    },
    "F51_Pursuit": {
        "id":  "http://iflastandards.info/ns/fr/frbr/frbroo/F51_Pursuit",
        "name": "Pursuit",
        "description": "This class comprises periods of continuous activity of an Actor in a specific professional or creative domain or field."
    }
}