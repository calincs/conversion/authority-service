const PORT = process.env.PORT || 5000;

const service_url = (process.env.GITLAB_ENVIRONMENT_URL || ("http://127.0.0.1:" + PORT));
const preview_url = service_url + "/preview?id={{id}}";

const manifest = {
  name: "LINCS Entity Reconciliation Service",
  identifierSpace: "http://id.lincsproject.ca/",
  versions: ["0.1", "0.2"],
  schemaSpace: "http://www.cidoc-crm.org/cidoc-crm/",
  defaultTypes: [
    {
      id: "http://www.cidoc-crm.org/cidoc-crm/E39_Actor",
      name: "crm:E39_Actor"
    },
    {
      id: "http://www.cidoc-crm.org/cidoc-crm/E21_Person",
      name: "crm:E21_Person"
    },
    {
      id: "http://www.cidoc-crm.org/cidoc-crm/E74_Group",
      name: "crm:E74_Group"
    },
    {
      id: "http://www.cidoc-crm.org/cidoc-crm/E70_Thing",
      name: "crm:E70_Thing"
    },
    {
      id: "http://www.cidoc-crm.org/cidoc-crm/E53_Place",
      name: "crm:E53_Place"
    },
    {
      id: "http://www.cidoc-crm.org/cidoc-crm/E55_Type",
      name: "crm:E55_Type"
    },
    {
      id: "http://www.cidoc-crm.org/cidoc-crm/E5_Event",
      name: "crm:E5_Event"
    }
  ],
  suggest: {
    type: {
      service_path: "/suggest/type",
      service_url: service_url
    },
    property: {
      service_path: "/suggest/property",
      service_url: service_url
    },
    entity: {
      service_path: "/suggest/entity",
      service_url: service_url
    }
  },
  preview: {
    url: preview_url,
    width: 450,
    height: 150
  }
};

export default manifest;
