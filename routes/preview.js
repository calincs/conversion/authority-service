import express from 'express';
import { LINCS_RESOLVE_PATH, LINCS_ENTITY_PREFIX, isValidURL } from '../index.js';
import { execQuery } from '../helpers/preview-query.js';

const router = express.Router();

router.get('/', (req, res) => {
    if("id" in req.query) {
        let uri;
        if (isValidURL(req.query.id)) {
            uri = decodeURIComponent(req.query.id);
        } else {
            uri = LINCS_ENTITY_PREFIX + req.query.id;
        }
        execQuery(uri).then((response) => {
            if(response.length == 0) {
                console.log('Error: invalid/non-existent id provided.');
                res.status(400).json("Error: invalid/non-existent id provided.");
            } else {
                // Extract entity data
                const link = LINCS_RESOLVE_PATH + response[0].uri;
                const labels = Array.from(new Set(response.map(entity => entity.label)));
                const types = Array.from(new Set(response.map(entity => entity.type)));
                const type_labels = Array.from(new Set(response.map(entity => entity.typeLabel)));
                const type_links = types.map(type => LINCS_RESOLVE_PATH + type);

                if(labels.length == 1) {
                    res.render('preview', {
                        entity_link: link,
                        entity_label: labels[0],
                        entity_id: req.query.id,
                        entity_types: types,
                        type_labels: type_labels,
                        type_links: type_links,
                        multilabel: false
                    });
                }
                else {
                    const alt_labels = labels.slice(1);
                    res.render('preview', {
                        entity_link: link,
                        entity_label: labels[0],
                        entity_id: req.query.id,
                        entity_types: types,
                        type_labels: type_labels,
                        type_links: type_links,
                        multilabel: true,
                        alternate_labels: alt_labels
                    });
                }
            }
        })
    } else {
        res.status(400).json("Error: the 'id' query parameter must be provided in the URL.");
    }
});

export default router;