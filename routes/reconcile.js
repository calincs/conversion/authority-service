import express, { response } from 'express';
import manifest from '../reconcile-manifest.js';
import { execQuery, execBatch } from '../helpers/reconcile-query.js';

/* 
spec: https://reconciliation-api.github.io/specs/latest/#structure-of-a-reconciliation-query
*/

const router = express.Router();

function handleGetRequest(req, res, namespace) {
  /*  Handles get requests as per the API specification listed at the top of this file.
      Args:   req         - HTTP request object
              res         - HTTP response object
              namespace   - namespace of URI's to be returned in query (optional)
  */
  if (req.query.queries) {
    handlePostRequest(req, res, namespace);
  } else {
    // Send manifest if no query is provided to reconcile against
    res.json(manifest);
  }
}

function handlePostRequest(req, res, namespace) {
  /*  Handles post requests as per the API specification listed at the top of this file.
      Args:   req         - HTTP request object
              res         - HTTP response object
              namespace   - namespace of URI's to be returned in query (optional)
  */
  let query_object;
  if ("queries" in req.body) {
    query_object = JSON.parse(req.body.queries);
  } else if (req.query.queries) {
    query_object = JSON.parse(req.query.queries);
  } else {
    res.status(400);
    res.json("Error: POST request incorrectly formatted. Assure queries are encoded within a form element named 'queries' with 'x-www-form-urlencoded bodies'.");
    return 0;
  }

  try {
    if ("query" in query_object) {                              // Single query
      execQuery(query_object, namespace).then((response) => {
        res.json(response);
      })
    } else {                                                    // Batch query
      execBatch(query_object, namespace).then((response) => {
        res.json(response);
      })
    }
  } catch (error) {
    res.status(500);
    res.send(error);
  }
}

router.get('/', (req, res) => {
  const namespace = "http://id.lincsproject.ca/";
  handleGetRequest(req, res, namespace);

});

router.post('/', (req, res) => {
  const namespace = "http://id.lincsproject.ca/";
  handlePostRequest(req, res, namespace);
});

router.get('/any', (req, res) => {
  handleGetRequest(req, res);
});

router.post('/any', (req, res) => {
  handlePostRequest(req, res);
})

export default router;