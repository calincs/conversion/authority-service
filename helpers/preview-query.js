import fetch from 'node-fetch';
import { SPARQL_URL } from '../index.js';

export async function execQuery(uri) {
    /*  Executes a query against the LINCS KG endpoint yielding a resource's label and type.

        Args: uri - URI of the LINCS entity for which the label is to be fetched
    */
    const query = `
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
        SELECT DISTINCT ?resource ?label ?type (SAMPLE(?tLabel) AS ?typeLabel) 
        WHERE 
        {
            BIND (<${uri}> AS ?resource)
            ?resource rdfs:label ?label ;
                rdf:type ?type .
            OPTIONAL {
                        ?type rdfs:label ?tLabel
                        FILTER (
                        LANGMATCHES(LANG(?tLabel), "en")|| (LANGMATCHES(LANG(?tLabel), "")))
                      }
        }
        GROUP BY ?resource ?label ?type
        `
    const params = {
        headers: { "Accept": "application/json" },
        method: "GET"
    }
    try {
        const response = await fetch(SPARQL_URL + new URLSearchParams({ query: query }), params);
        var data = await response.json();
    }
    catch (err) {
        const errorStr = `[-] ERROR: ${err}`;
        console.log(errorStr);
        return errorStr;
    }
    
    let results = [];
    // Format returned data
    for (const [key, obj] of Object.entries(data.results.bindings)) {
        // Type labels are set to optional in the SPARQL query
        var type_label = 'null';
        if ("typeLabel" in obj) {
          if ("value" in obj.typeLabel) {
            type_label = obj.typeLabel.value;
          }
        }
        
        results.push({
            "uri": obj.resource.value,
            "label": obj.label.value,
            "type": obj.type.value,
            "typeLabel": type_label,
        })
    }
    return results;
}