import fetch from 'node-fetch';
import { SPARQL_URL } from '../index.js';
import { PREFIX, PREFIXES } from './prefixes.js';

export async function execPropertiesQuery(prefix, type = null) {
  /*  
  Executes a query against the LINCS Fuseki endpoint to retrieve all properties of a given type (if provided) matching the provided prefix (str) in one way or another.

  Args:   prefix - a string (inputted by user via OpenRefine) which will be used to filter properties
          type (optional) - the full URI of the entity type for which properties are desired
  */

  let query = PREFIXES + `
    SELECT ?property (SAMPLE(?pl) as ?propertyLabel)
    WHERE 
    {
      ?property text:query (rdfs:label skos:altLabel skos:prefLabel foaf:name '${prefix} ${prefix}*' 1000) .
      ?subject ?property ?object .
      OPTIONAL {
        ?property rdfs:label ?pl
        FILTER (
          LANGMATCHES(LANG(?pl), "en")
          || (LANGMATCHES(LANG(?pl), ""))
        )
      }
  `;
  // Filter by type if declared
  if (type) {
    // KEEP FOR FUTURE SWITCH - TODO
    // In the future, we want to query only for properties that are possible for the type defined by the OpenRefine user.
    // However, there is a bug in OpenRefine right now where it forces a type into the query even if the user un-selected type
    // Temporarily we will use the same query regardless of whether a type is defined
    // Once the OpenRefine bug is fixed, we should switch the query back. Leaving that query here commented for now

    // query += `
    //     ?subject rdf:type <${type}> ;
    //         ?property ?object .
    // `
  }
  query += `
    }
    GROUP BY ?property
  `;
  const params = {
    headers: { "Accept": "application/json" },
    method: "GET"
  }
  try {
    const response = await fetch(SPARQL_URL + new URLSearchParams({ query: query }), params);
    var data = await response.json();
  }
  catch (err) {
    const errorStr = `[-] ERROR: ${err}`;
    console.log(errorStr);
    return errorStr;
  }

  let candidate_properties = [];
  // Format returned data
  for (const [key, obj] of Object.entries(data.results.bindings)) {
    candidate_properties.push({
      "id": obj.property.value,
      "name": ('propertyLabel' in obj ? obj.propertyLabel.value : 'undefined')
    });
  }

  // Add custom graph property
  if (String(prefix).toLowerCase().startsWith('g')) {
    candidate_properties.push({
      "id": 'http://graph.lincsproject.ca/',
      "name": 'named graph'
    });
  }

  return candidate_properties;
}


export async function execEntitiesQuery(prefix, type = null) {
  /*  
  Executes a query against the LINCS Fuseki endpoint to retrieve all entities of a given type (if provided) matching the provided prefix string in one way or another.

  Args:   prefix - a string which will be used to filter entities
          type - the full URI indicating which type of entities to return (if not specified will return all types of entities)
  */

  let query = PREFIXES + `
    SELECT DISTINCT ?resource (SAMPLE(?rl) as ?label)
    WHERE 
    {
      ?resource text:query (rdfs:label skos:altLabel skos:prefLabel foaf:name '${prefix} ${prefix}*' 1000) .
  `;

  // If type declared then limit entities to specified class or it's sub-classes
  if (type) {
    query += `
      { 
        ?resource rdf:type <${type}> .
      } 
      UNION 
      { 
        ?sub_type rdfs:subClassOf+ <${type}> .
        ?resource rdf:type ?sub_type .
      }`;
  }

  // Limit to LINCS entities - this can be changed later with an optional parameter if desired
  // query += `
  //   FILTER (strstarts(str(?resource), "http://id.lincsproject.ca/"))
  // `;

  query += `
      OPTIONAL {
        ?resource rdfs:label ?rl
        FILTER (
          LANGMATCHES(LANG(?rl), "en")
          || (LANGMATCHES(LANG(?rl), ""))
        )
      }
    }
    GROUP BY ?resource
  `;

  const params = {
    headers: { "Accept": "application/json" },
    method: "GET"
  }
  try {
    console.log(query);
    const response = await fetch(SPARQL_URL + new URLSearchParams({ query: query }), params);
    var data = await response.json();
  }
  catch (err) {
    const errorStr = `[-] ERROR: ${err}`;
    console.log(errorStr);
    return errorStr;
  }

  let candidate_properties = [];
  // Format returned data
  for (const [key, obj] of Object.entries(data.results.bindings)) {
    candidate_properties.push({
      "id": obj.resource?.value,
      "name": obj.label?.value,
    });
  }
  return candidate_properties;

}