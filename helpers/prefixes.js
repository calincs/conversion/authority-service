const lincs = "http://id.lincsproject.ca/";
const crm = "http://www.cidoc-crm.org/cidoc-crm/";
const foaf = "http://xmlns.com/foaf/0.1/";
const frbroo = "http://iflastandards.info/ns/fr/frbr/frbroo/";
const owl = "http://www.w3.org/2002/07/owl#";
const rdf = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";
const rdfs = "http://www.w3.org/2000/01/rdf-schema#";
const schema = "http://schema.org/";
const skos = "http://www.w3.org/2004/02/skos/core#";
const text = "http://jena.apache.org/text#";
const wd = "http://www.wikidata.org/entity/";

export const PREFIX = {
  lincs: lincs,
  crm: crm,
  foaf: foaf,
  frbroo: frbroo,
  owl: owl,
  rdf: rdf,
  rdfs: rdfs,
  schema: schema,
  skos: skos,
  text: text,
  wd: wd
}

function createPrefixes() {
  let result = "";
  Object.keys(PREFIX).forEach(function (key) {
    result += `PREFIX ${key}: <${PREFIX[key]}> `;
  });
  return result.slice(0, -1);
}

export const PREFIXES = createPrefixes();

// Replace prefixes with keys
export function ReducePrefixes(input) {
  let result = input.valueOf();
  Object.keys(PREFIX).forEach(function (key) {
    result = result.replace(PREFIX[key], key + ":");
  });
  return result;
}