import express from 'express';
import YAML from 'yamljs';
import swaggerUI from 'swagger-ui-express';
import bodyParser from 'body-parser';
import cors from 'cors';

import reconcileRoutes from './routes/reconcile.js';
import previewRoutes from './routes/preview.js';
import suggestRoutes from './routes/suggest.js';

export const SPARQL_URL = process.env.SPARQL_ENDPOINT || "https://fuseki.lincsproject.ca/lincs/sparql?";
export const LINCS_RESOLVE_PATH = process.env.RESOLVE_URL || "https://rs-review.lincsproject.ca/resource/?uri=";
export const LINCS_ENTITY_PREFIX = process.env.LINCS_ENTITY_PREFIX || "http://id.lincsproject.ca/";
const PORT = process.env.PORT || 5000;
export function isValidURL(str) {
    /*
    Tests if a string is a valid URL using the URL Constructor (https://developer.mozilla.org/en-US/docs/Web/API/URL).
    */
    let url;
    try {
        url = new URL(str);
    } catch (_) {
        return false;
    }
    return true;
}

const app = express();

// Enable CORS for ALL requests (using cors middleware prior to configuring routes)
app.use(cors());
// Parse incoming Request Object if object, with nested objects, or generally any type
app.use(bodyParser.urlencoded({
  extended: true
}));
// Set template engine to pug (for HTML templates used in preview service)
app.set('views', './views');
app.set('view engine', 'pug');

app.use('/reconcile', reconcileRoutes);
app.use('/preview', previewRoutes);
app.use('/suggest', suggestRoutes)

const swaggerDocument = YAML.load('./openapi.yaml');
var options = {
  customCss: '.swagger-ui .topbar { display: none }'
};
app.use('/api-docs', swaggerUI.serve, swaggerUI.setup(swaggerDocument, options));
app.get('/', (req, res) => res.redirect("/api-docs"));

app.listen(PORT, () => {
  console.log(`[+] Server running on port ${PORT}`)
});