openapi: 3.0.3
info:
  title: LINCS Authority Service
  description: |-
    This is the LINCS Authority service for entity reconciliation and lookups.
  version: 1.0.0

servers:
  - url: https://authority.lincsproject.ca/
    description: LINCS production server
  - url: http://localhost:5000/
    description: local dev server

tags:
  - name: Entity Lookup
    description: Read-only entity endpoint
  - name: Reconciliation
    description: Entity reconciliation endpoints for OpenRefine
  - name: Suggestions
    description: Auto-complete endpoints for entities, properties, and types utilized by OpenRefine

paths:

  /reconcile/any:
    post:
      tags:
        - Reconciliation
      summary: Returns candidate entities that exist in the LINCS triplestore
      description: By default, this will only return entity URIs with namespace http://id.lincsproject.ca/. The /reconcile/any/ endpoint is identical to /reconcile/, but with the namespace constraint removed.
      requestBody:
        required: true
        content:
          application/x-www-form-urlencoded:
            schema:
              $ref: "#/components/schemas/ReconcileQuery"
      responses:
        "200":
          description: Returns a list of potential candidates
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Candidates"
  
  /preview:
    get:
      tags:
        - Reconciliation
      summary: Returns an HTML element containing an entity's label(s) and type(s) as hyperlinks that redirect to LINCS ResearchSpace
      parameters:
        - in: query
          name: id
          schema:
            example: http://id.lincsproject.ca/NyLaHOma0gC
            type: string 
          required: true
          description: The URI of the entity to preview
      
      responses:
        "200":
          description: HTML preview of a given entity
          content:
            text/html:
              schema:
                $ref: "#/components/schemas/Preview"
  
  /suggest/type:
    get:
      tags:
        - Suggestions
      summary: Returns candidate types based on a provided query.
      parameters:
        - in: query
          name: prefix
          schema:
            example: act
            type: string
          required: true
          description: Approximate string used to filter all possible types. 

      responses:
        "200":
          description: A list of candidate types.
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/SuggestTypes"

  /suggest/property:
    get:
      tags:
        - Suggestions
      summary: Returns candidate properties based on a provided query string and entity type.
      parameters:
        - in: query
          name: prefix
          schema:
            example: parent
            type: string
          required: true
          description: A string used to filter all possible properties.
        - in: query
          name: type
          schema:
            example: http://www.cidoc-crm.org/cidoc-crm/E39_Actor
            type: string
          required: false
          description: An entity type used to further filter properties (only properties of this type will be returned).

      responses:
        "200":
          description: A list of candidate properties.
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/SuggestProperties"

  /suggest/entity:
    get:
      tags:
        - Suggestions
      summary: Returns candidate entities based on a provided query and optionally type.
      parameters:
        - in: query
          name: prefix
          schema:
            example: Mar
            type: string
          required: true
          description: Approximate string used to filter all possible entities.
        - in: query
          name: type
          schema:
            example: http://www.cidoc-crm.org/cidoc-crm/E21_Person
            type: string
          required: false
          description: A type used to further filter entities (only entites of this type or sub-types will be returned).

      responses:
        "200":
          description: A list of candidate entities.
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/SuggestEntities"



components:
  schemas:

    Preview:
      type: string
      example: |
        <html>
          <body style="margin: 0px; font-family: Arial; sans-serif">
            <div style="font-size: 0.8em">
              <a href="https://rs-review.lincsproject.ca/resource/?uri=http://id.lincsproject.ca/NyLaHOma0gC" target="_blank">John Fox</a>
              <br><span style="color: #505050;"> (http://id.lincsproject.ca/NyLaHOma0gC)</span><br>
              <br><type><a href="https://rs-review.lincsproject.ca/resource/?uri=http://www.cidoc-crm.org/cidoc-crm/E21_Person" target="_blank">E21 Person</a></type><br>
              <span style="color: #505050;"> (http://www.cidoc-crm.org/cidoc-crm/E21_Person)</span><br>
              <br>
            </div>
          </body>
        </html>

    SuggestTypes:
      type: object
      properties:
        results:
          type: list
          example:
            - id: http://www.cidoc-crm.org/cidoc-crm/E7_Activity 
              name: Activity
              description: This class comprises actions intentionally carried out by instances of E39 Actor that result in changes of state in the cultural, social, or physical systems documented.
            - id: http://www.cidoc-crm.org/cidoc-crm/E39_Actor
              name: Actor
              description: This class comprises people, either individually or in groups, who have the potential to perform intentional actions of kinds for which someone may be held responsible.
            - id: http://www.cidoc-crm.org/cidoc-crm/E87_Curation_Activity
              name: Curation Activity
              description: This class comprises the activities that result in the continuity of management and the preservation and evolution of instances of E78 Curated Holding, following an implicit or explicit curation plan.

    SuggestProperties:
      type: object
      properties:
        results:
          type: list
          example:
            - id: http://www.w3.org/1999/02/22-rdf-syntax-ns#type
              name: type
            - id: http://www.cidoc-crm.org/cidoc-crm/P2_has_type 
              name: has type
    
    SuggestEntities:
      type: object
      properties:
        results:
          type: list
          example:
            - id: http://id.lincsproject.ca/2zOPqwIPx8A
              name: Dufferin and Ava, Basil Sheridan Hamilton-Temple-Blackwood,,, fourth Marquess of
            - id: http://id.lincsproject.ca/Z1FI00idqA7
              name: Grey, Mary,,, Lady
            - id: http://id.lincsproject.ca/x1kPGe31wmr
              name: Willoughby, Margaret

    ReconcileQuery:
      type: object
      properties:
        queries:
          type: object
          properties:
            q1:
              type: object
              properties:
                query:
                  required: true
                  type: string
                  example: John
                type:
                  required: false
                  type: string
                  example: http://www.cidoc-crm.org/cidoc-crm/E39_Actor
            q2:
              type: object
              properties:
                query:
                  required: true
                  type: string
                  example: Mary Ann Shadd
                limit:
                  required: false
                  type: integer
                  example: 3
    
    Candidates:
      type: array
      items:
        type: object
        properties:
          id:
            type: string
            example: http://id.lincsproject.ca/NyLaHOma0gC
          type:
            type: array
            items:
              type: object
              properties:
                id:
                  type: string
                  example: http://www.cidoc-crm.org/cidoc-crm/E21_Person
                name:
                  type: string
                  example: E21 Person
          name:
            type: string
            example: John Fox
          score:
            type: integer
            example: 94
          links:
            type: integer
            example: 4
          graph:
            type: string
            example: http://graph.lincsproject.ca/orlando
