// helper functions to parse and convert query output to common Statements

// converts entity query results to common Statements
export function toStatements(entities) {
  let results = [];
  for (const entity of entities) {
    let statement = extractLabelAndType(entity);
    if (entity.events) {
      statement.incoming = [];
      for (const event of entity.events) {
        statement.incoming.push(extractLabelAndType(event));
      }
    }
    results.push(statement);
  }
  return results;
}

// returns a Simple Resource with type and label properties extracted from triples[]
function extractLabelAndType(entity) {
  let result = { resource: entity.resource }
  let triples = [];
  for (const triple of entity.triples) {
    if (triple.predicate == "http://www.w3.org/2000/01/rdf-schema#label") {
      result.resourceLabel = triple.object;
      result.graph = triple.graph;
    }
    else if (triple.predicate == "http://www.w3.org/1999/02/22-rdf-syntax-ns#type") {
      result.type = triple.object;
      result.typeLabel = triple.objectLabel;
    }
    else {
      triples.push(triple);
    }
  }
  if (triples && triples.length > 0)
    result.outgoing = triples;
  return result;
}