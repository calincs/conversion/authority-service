import fetch from "node-fetch";
import { SPARQL_URL, isValidURL } from "../index.js";
import { PREFIX, PREFIXES } from "./prefixes.js";

function prefixParse(str) {
  /*  Parses the provided string into an acceptable format for full text search 
      Args: str - string to be formatted
      Returns: formatted string
  */
  let string = String(str);
  // const len = string.length;
  // const percentage = 0.7;
  // const longer = 6;
  // if (len >= longer) {
  //   string = string.slice(0, parseInt(percentage * len));
  //   string += "*";
  // }
  // replace all non-alphanumeric characters with * (except [#,?!])
  string = string.replace(/[^\w #\/,\?\!]/gi, "*");
  return string;
}

function buildQuery(query_string, type, properties, namespace) {
  /*  Builds a query to run against LINCS KG endpoint.
      Args: query_string - query string, to search for entities with similar names
            type - URI representing type of entity to search for
            properties - an array of object(s), where each object maps a property identifier ('pid' field) to a property value ('v' field)
            namespace - filter candidates whose resource identifier starts with the string passed by this argument
          
      Returns: a query string to pass to a SPARQL endpoint
   */

  // Prepare search term to optimise Fuseki index search
  let searchTerm = String(query_string);
  if (searchTerm.includes(" ")) {
    let tokens = searchTerm.split(" ");
    var i = 0;
    while (i < tokens.length) {
      if (tokens[i].length === 1 && tokens[i] != "i" && tokens[i] != "a") {
        tokens.splice(i, 1); // remove illegal single characters
      } else {
        tokens[i] = prefixParse(tokens[i]); // ensure length and remove illegal chars
        ++i;
      }
    }
    searchTerm = tokens.join(" ");
  } else {
    searchTerm = prefixParse(searchTerm);
  }

  let query = `   
    PREFIX rdf: <${PREFIX.rdf}>
    PREFIX rdfs: <${PREFIX.rdfs}>
    PREFIX crm: <${PREFIX.crm}>
    PREFIX text: <${PREFIX.text}>
    PREFIX skos: <${PREFIX.skos}>
    PREFIX foaf: <${PREFIX.foaf}>
    SELECT ?resource (COALESCE(?pLabel, ?rLabel) AS ?label) ?type (SAMPLE(?tLabel) AS ?typeLabel) ?g
      (SAMPLE(?cnt) AS ?links) ?score WHERE {
      (?resource ?score ?lit ?g) text:query (rdfs:label skos:altLabel skos:prefLabel foaf:name '${searchTerm}' 50) .`;

  // Limit entities to given class or sub-classes if provided (e.g. http://www.cidoc-crm.org/cidoc-crm/E21_Person)
  if (type) {
    if (Array.isArray(type)) {
      // If multiple types provided
      type.forEach((t, index) => {
        if (index) {
          // No 'UNION' prefix if first type definition
          query += `
            UNION
          `;
        }
        query += `
          { 
            ?resource rdf:type <${t}> .
          } 
          UNION 
          { 
            ?sub_type rdfs:subClassOf+ <${t}> .
            ?resource rdf:type ?sub_type .
          }
        `;
      });
    } else {
      query += `
        { 
          ?resource rdf:type <${type}> .
        } 
        UNION 
        { 
          ?sub_type rdfs:subClassOf+ <${type}> .
          ?resource rdf:type ?sub_type .
        }
      `;
    }
  }

  // Append property specification if provided (unless parent graph property)
  if (properties) {
    properties.forEach((property) => {
      if (property.pid !== "http://graph.lincsproject.ca/") {
        // If property value is a URI then encase value with '<' and '>' otherwise do not
        if (isValidURL(String(property.v))) {
          query += `
            ?resource <${property.pid}> <${property.v}> .   
          `;
        } else {
          query += `
            ?resource <${property.pid}> '${property.v}' .
          `;
        }
      }
    });
  }

  // If namespace is declared (e.g. http://id.lincsproject.ca/) filter accordingly
  if (namespace) {
    query += ` 
      FILTER (strstarts(str(?resource), "${namespace}")) .
    `;
  }

  query += `
      OPTIONAL {
        ?resource rdfs:label ?rLabel .
        FILTER (
          LANGMATCHES(LANG(?rLabel), "en")
          || (LANGMATCHES(LANG(?rLabel), ""))
        )
      }
      OPTIONAL {
        ?resource skos:prefLabel ?pLabel .
        FILTER (
          LANGMATCHES(LANG(?pLabel), "en")
          || (LANGMATCHES(LANG(?pLabel), ""))
        )
      }
      ?resource rdf:type ?type .
      OPTIONAL {
        ?type rdfs:label ?tLabel .
        FILTER (
          LANGMATCHES(LANG(?tLabel), "en")
          || (LANGMATCHES(LANG(?tLabel), ""))
        )
      }
      OPTIONAL {
        ?resource <http://id.lincsproject.ca/lincs/connectionCount> ?cnt
      }`;

  // Filter by graph only if named graph is passed in properties
  let graphFilter = "";
  if (properties) {
    properties.forEach((property) => {
      if (property.pid === "http://graph.lincsproject.ca/") {
        let named_graph;
        if (isValidURL(property.v)) {
          named_graph = `<${property.v}>`;
        } else {
          named_graph = `<${property.pid}${property.v}>`;
        }
        graphFilter += `?g = ${named_graph}
          || `;
      }
    });
  }
  if (graphFilter) {
    query += `
      FILTER(${graphFilter.slice(0, -4)})`;
  }

  query += `
    }
    GROUP BY ?resource ?score ?g ?type ?pLabel ?rLabel
    ORDER BY DESC(?score)`;

  return query;
}

export async function execQuery(query_object, namespace = null) {
  /*  Builds a query based on the information passed through query_object and executes it against the LINCS KG endpoint
      Args:   query_object with the following keys (only the query key/value is necessary, the rest are optional)
                  query - a non-empty query string, used to search for entities with similar names
                  type  - a URI indicating what type of entities to search for (i.e. person or the more general actor type)
                  limit - a positive integer representing the maximum number of candidates to return
                  namespace - filter candidates whose resource identifier contains the string passed by this argument
      Returns: an array of possible candidates
   */

  let results = [];
  const queryString = query_object.query;
  const type = "type" in query_object ? query_object.type : null;
  const properties = "properties" in query_object ? query_object.properties : null;
  const limit = "limit" in query_object ? query_object.limit : 20;

  const query = buildQuery(queryString, type, properties, namespace);

  const params = {
    headers: { Accept: "application/json" },
    method: "GET",
  };
  try {
    console.log("reconcile query: " + query);
    const response = await fetch(SPARQL_URL + new URLSearchParams({ query: query }), params);
    var data = await response.json();
  } catch (err) {
    const errorStr = `[-] ERROR: ${err}`;
    console.log(errorStr);
    return errorStr;
  }

  const rows = Object.entries(data.results.bindings)
  // first find the max number of links and max index score per candidate
  var maxLinks = 0;
  var maxIndex = 0;
  for (const [key, obj] of rows) {
    const links = parseInt(obj.links?.value);
    const index = parseFloat(obj.score.value);
    if (maxLinks < links) maxLinks = links;
    if (maxIndex < index) maxIndex = index;
  }

  // Order rows by "resource" and then by "label" (reverse)
  rows.sort((a, b) => {
    if (a[1].resource?.value === b[1].resource?.value) {
      return a[1].pLabel?.value > b[1].pLabel?.value ? 1 : -1;
    }
    return a[1].resource?.value < b[1].resource?.value ? -1 : 1;
  });
  
  const entities = new Set();
  // Format returned data
  for (const [key, obj] of rows) {
    // Skip if duplicate
    const setSize = entities.size;
    entities.add(obj.resource.value);
    if (setSize === entities.size) {
      continue;
    }

    // Skip if object does not have a label or type
    if (obj.label === undefined || obj.type === undefined) {
      continue;
    }

    /*
      Score calculation
      We are currently calculating a match score that is relative to the candidates within the current filter
      Also note that OpenRefine requires the score to be constant regardless of the batch it is sent with
    */

    /* Index Score only distance calculation */
    const indexRatio = parseFloat(obj.score.value) / maxIndex;
    const indexFactor = 25; // How much the text index score weighs (max distance)
    const totalDist = 5 + (1.0 - indexRatio) * indexFactor; // shift distance with a constant

    // Use comparative number of links to influence score
    let links = 0;
    if (obj.links) links = parseInt(obj.links.value);
    const linkRatio = links / (maxLinks + 1);
    const linkFactor = 4; // max influence on the score (4 means -2 to +2)
    // Score calculation (closer to 100 is better) - to be altered later if need be
    const score = totalDist > 90 ? 0 : parseInt(100 - totalDist + linkFactor * (linkRatio - 0.5));

    // Type labels are set to optional in the SPARQL query
    let type_label = "";
    if (obj.typeLabel) {
      type_label = obj.typeLabel.value;
    }

    // Encode reserved character '#' if present
    let identifier;
    if (obj.resource.value.includes("#")) {
      identifier = obj.resource.value.replaceAll("#", "%23");
    } else {
      identifier = obj.resource.value;
    }

    let record = {
      id: identifier,
      type: [
        {
          id: obj.type.value,
          name: type_label,
        },
      ],
      name: obj.label.value?.trim(),
      score: score,
      graph: obj.g.value,
    };
    if (obj.links) record.links = links;
    results.push(record);
  }

  // Sort array by decreasing score and return
  results.sort((a, b) => {
    return b.score - a.score;
  });
  return results.slice(0, limit);
}

export async function execBatch(query_object, namespace = undefined) {
  let result = {};

  for (const key of Object.keys(query_object)) {
    if ("query" in query_object[key]) {
      result[key] = {};
      result[key]["result"] = await execQuery(query_object[key], namespace);
    } else {
      return "Error: batch query incorrectly formatted.";
    }
  }
  return result;
}
