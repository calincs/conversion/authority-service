# Authority service

The [LINCS Authority Service](https://portal.lincsproject.ca/docs/tools/authority-service/) to be used by [OpenRefine](https://openrefine.org/) and LINCS tools.

## Using with OpenRefine

See [our documentation](https://portal.lincsproject.ca/docs/tools/authority-service/authority-service-documentation) for reconciling entities in your data against entities in the LINCS Knowledge Graph (KG) using OpenRefine.

For more general information about reconciling with OpenRefine, refer to the [OpenRefine documentation](https://openrefine.org/docs/manual/reconciling).

Our service follows the [W3C Reconciliation Service API specifications (v0.2)](https://www.w3.org/community/reports/reconciliation/CG-FINAL-specs-0.2-20230410/).

In addition to OpenRefine, the reconciliation endpoints are used by [NSSI](https://portal.lincsproject.ca/docs/tools/nssi/) and the [LINCS Context Plugin](https://portal.lincsproject.ca/docs/tools/context-explorer/).

## Using as a Context API

Our service can be used to get connected statements about entities in the LINCS Knowledge Graph. See this in action with the [LINCS Context Plugin](https://portal.lincsproject.ca/docs/tools/context-explorer/). 

## API Documentation

API Docs are available at https://authority.lincsproject.ca/api-docs/

## Setup

### Docker

`docker build --no-cache -t openrefine .`

`docker run -p 5000:5000 -d openrefine`

This should make the service accessible at `127.0.0.1:5000`  

The following environment variables can be modified (within the gitlab-ci yaml file):

- `PORT` : specifies the port on which the API is hosted (defaults to `5000`)
- `SPARQL_ENDPOINT` : specifies the base SPARQL endpoint which queries are executed against (defaults to `https://fuseki.lincsproject.ca/lincs/sparql?`)
- `RESOLVE_PATH` : specifies the base URL to which the hyperlink of reconciled entities will resolve to (defaults to rs-review for the time being which is `https://rs-review.lincsproject.ca/resource/?uri=`)

**NOTE:** If you do change the `PORT` env variable then you will have to modify the port mapping within the `docker run` command shown above accordingly.

For the preview service to work locally, make sure that the correct URI is set in `reconcile-manifest.js`.

### Locally hosted API

If you wish to host the API locally without using docker, you must have Node.js and npm installed (instructions for how to download these dependencies can be found [here](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm)).  
To see if you already have Node.js and npm installed run the following commands:

```bash
node -v  
npm -v
```  

Then follow these steps:  

1. Clone this repository on your machine
2. The API is hosted on local port 5000 by default. You can change this by setting an environmental variable named `PORT` to the desired port and then restarting the API.
3. Install the necessary dependencies by running `npm install` in the directory of the repository
4. Start the API by typing `npm start`  
5. Reconciliation queries must be sent to the `/reconcile` path (e.g. `http://127.0.0.1:5000/reconcile`). In order to send `POST` requests (for testing), you must use another application other than your browser (e.g. Postman). The data which is provided as input to the authority service must be formatted with `application/x-www-form-urlencoded` bodies containing a reconciliation query batch (serialized in JSON) in a form element named `queries`, as mentioned in the [API reconciliation specification](https://reconciliation-api.github.io/specs/latest/#sending-reconciliation-queries-to-a-service).

#### CURL Example requests

```bash
curl http://127.0.0.1:5000/reconcile --data-urlencode 'queries={"query": "John", "type": "http://www.cidoc-crm.org/cidoc-crm/E39_Actor", "limit": 5}'
```

#### Postman Example requests

Examples of both GET and POST requests for reconciliation queries and batch queries can be accessed by importing the collection within the `postman_examples.json` file provided in the repository.

## Resources

LINCS ResearchSpace UI: `https://rs-review.lincsproject.ca`

LINCS KG endpoint: `https://fuseki.lincsproject.ca/lincs/sparql?`
