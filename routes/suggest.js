import express from 'express';
import { crm_classes } from '../helpers/suggest-cache.js';
import { execPropertiesQuery, execEntitiesQuery } from '../helpers/suggest-query.js';

const router = express.Router();

function getClassMatches(query) {
    /*
    Returns all classes which match the provided 'query' (string) parameter.
    */
    let matches = [];
    Object.keys(crm_classes).forEach(type => {
        if (type.toLowerCase().includes(query.toLowerCase())) {
            matches.push(crm_classes[type]);
        }
    });
    return matches;
}

router.get('/', (req,res) => {
    res.json("Visit '/type', '/entity', and '/property' endpoints for corresponding suggestions.");
})

router.get('/type', (req,res) => {
    if('prefix' in req.query) {
        res.json({
            "result": getClassMatches(req.query.prefix)
        });
    } else {
        res.status(400).json("Error: the 'prefix' query parameter must be provided in the URL.");
    }
})

router.get('/property', (req,res) => {
    if('prefix' in req.query) {
        execPropertiesQuery(req.query.prefix, req.query.type).then((candidates) => {
            res.json({
                "result": candidates
            });
        })
    } else {
        res.status(400).json("Error: the 'prefix' query parameter must be provided in the URL.");
    }
})

router.get('/entity', (req,res) => {
    if('prefix' in req.query) {
        execEntitiesQuery(req.query.prefix, req.query.type).then((candidates) => {
            res.json({
                "result": candidates
            });
        })
    } else {
        res.status(400).json("Error: the 'prefix' query parameter must be provided in the URL.");
    }
})





export default router;